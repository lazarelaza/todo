﻿
using Microsoft.EntityFrameworkCore;

// Database context - glavna klasa sto ja koordinira Entity Framework funkcionialnosta za data modelot. 
//Microsoft.EntityFrameworkCore.DbContext klasa

namespace TodoApi.Model
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }
    }
}
