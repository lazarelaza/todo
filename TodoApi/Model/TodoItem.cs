﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//Model - objekt koj gi pretstavuva podatocite na aplikacijata t.e gi cuva podatocite dobieni od database
//Modelite se C# klasi 

namespace TodoApi.Model
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Complete { get; set; }
    }
}
