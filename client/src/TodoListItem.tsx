import React from 'react'
import "./css/TodoListItem.css"

//props se pisuva vo interface ili types
interface TodoListItemProps {
    todo: {name: string; complete: boolean};
    checkUncheck: (selectedTodo: Todo) => void;

}
 
//export const TodoListItem = props =>
export const TodoListItem: React.FC<TodoListItemProps> = ({todo, checkUncheck}) => {
    return (
        <li>
            <label style={{textDecoration: todo.complete ? "line-through" : "none"}}>
                <input type="checkbox" checked={todo.complete} 
                onChange={() => checkUncheck(todo)} />
                {todo.name}
            </label>
        </li>

    )
}