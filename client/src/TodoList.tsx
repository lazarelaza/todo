import React from "react";
import { TodoListItem } from "./TodoListItem";

interface TodoListProps {
    todos: Array<Todo>;
    checkUncheck: (selectedTodo: Todo) => void;

}

export const TodoList: React.FC<TodoListProps> = ({todos, checkUncheck}) =>{
    return (
    <ul>
        {todos.map(todo => {
            return (
            <TodoListItem key={todo.name} todo={todo} checkUncheck={checkUncheck} />
        )
        })}
    </ul>
    )
}