import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TodoList } from './TodoList';
import { AddTodoForm } from './AddTodoForm';

type Todo = {
  name: string;
  complete: boolean;
};

//inicijalizacija na todo taskovite
const initialTodos: Array<Todo> = [];

const App: React.FC = () => {
  //const [var1, var2] -> Destructuring assignment
  //i todos i setTodos ke gi koristat initialTodos
const [todos, setTodos] = useState(initialTodos);

const checkUncheck = (selectedTodo: Todo) => {
  const newTodos = todos.map(todo => {
    if(todo === selectedTodo){
      return{
        ...todo,
        complete: !todo.complete
      }
    }
return todo;
  })
  setTodos(newTodos)
}

const addTodo = (newTodo: string) => {
  newTodo.trim() !== "" &&
  setTodos([...todos, {name: newTodo, complete: false}])
}

useEffect(() => {
  const fetchData = async () => {
    const getTodo = await axios.get('https://localhost:5001/api/TodoItems');
    setTodos(getTodo.data);
  }
  fetchData();
}, []);

  return (
    <React.Fragment>
    <TodoList todos={todos} checkUncheck={checkUncheck}/>
    <AddTodoForm addTodo={addTodo}/>
    </React.Fragment> 
  )
};

export default App;