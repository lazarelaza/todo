type Todo = {
    name: string;
    complete: boolean;
  };

  type ChangeTodo = (selectedTodo: Todo) => void;
  
  type AddTodo = (newTodo: string) => void;